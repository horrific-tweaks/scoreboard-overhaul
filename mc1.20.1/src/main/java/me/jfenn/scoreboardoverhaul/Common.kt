package me.jfenn.scoreboardoverhaul

import net.fabricmc.loader.api.FabricLoader

fun hasClothConfig(): Boolean {
    return FabricLoader.getInstance().isModLoaded("cloth-config")
}
