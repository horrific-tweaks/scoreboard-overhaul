package me.jfenn.scoreboardoverhaul

import me.jfenn.scoreboardoverhaul.common.hud.HudRenderer
import me.jfenn.scoreboardoverhaul.common.hud.HudState
import me.jfenn.scoreboardoverhaul.impl.*
import net.fabricmc.api.ClientModInitializer
import org.slf4j.LoggerFactory

object Main : ClientModInitializer {

    private val log = LoggerFactory.getLogger(this::class.java)

    val configFactory = if (hasClothConfig()) {
        ScoreboardConfigFactory
    } else {
        log.error("cloth-config is not installed! Scoreboard Overhaul cannot create its config menu without it!")
        ScoreboardConfigDummyFactory
    }

    override fun onInitializeClient() {
        log.info("Starting Scoreboard Overhaul client")

        val scoreboardAccessor = ScoreboardAccessor()
        val hudState = HudState(configFactory, scoreboardAccessor, OptionsAccessor)
        val hudRenderer = HudRenderer(configFactory, hudState, OptionsAccessor, HudCallbackImpl(DrawService))

        hudState.init()
        hudRenderer.init()
    }
}