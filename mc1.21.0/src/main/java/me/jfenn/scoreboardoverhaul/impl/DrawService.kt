package me.jfenn.scoreboardoverhaul.impl

import me.jfenn.scoreboardoverhaul.api.IDrawService
import me.jfenn.scoreboardoverhaul.api.IDrawServiceFactory
import net.minecraft.client.gui.DrawContext
import net.minecraft.util.Identifier

class DrawService(
    override val context: DrawContext,
) : IDrawService {

    companion object : IDrawServiceFactory {
        override fun create(drawContext: DrawContext): IDrawService = DrawService(drawContext)
    }

    override fun drawTexture(
        texture: Identifier,
        x: Int,
        y: Int,
        u: Float,
        v: Float,
        width: Int,
        height: Int,
        textureWidth: Int,
        textureHeight: Int
    ) {
        context.drawTexture(
            texture.let { Identifier.of(it.namespace, "textures/gui/sprites/${it.path}.png")!! },
            x, y,
            u, v,
            width, height,
            textureWidth, textureHeight
        )
    }

}