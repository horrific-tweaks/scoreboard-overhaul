package me.jfenn.scoreboardoverhaul.impl

import me.jfenn.scoreboardoverhaul.api.IScoreboardAccessor
import me.jfenn.scoreboardoverhaul.common.data.ObjectiveInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreRenderType
import net.minecraft.client.MinecraftClient
import net.minecraft.scoreboard.ScoreboardCriterion
import net.minecraft.scoreboard.ScoreboardDisplaySlot
import net.minecraft.scoreboard.ScoreboardEntry
import net.minecraft.scoreboard.Team
import net.minecraft.scoreboard.number.BlankNumberFormat
import net.minecraft.scoreboard.number.FixedNumberFormat
import net.minecraft.scoreboard.number.NumberFormat
import net.minecraft.scoreboard.number.StyledNumberFormat
import net.minecraft.util.Formatting

class ScoreboardAccessor : IScoreboardAccessor {
    override fun getSidebarObjective(): ObjectiveInfo? {
        val client = MinecraftClient.getInstance()
        val scoreboard = client.world?.scoreboard ?: return null
        val player = client.player ?: return null

        val teamSlot = player.scoreboardTeam?.color?.let { ScoreboardDisplaySlot.fromFormatting(it) }
        val teamObjective = teamSlot?.let {
            scoreboard.getObjectiveForSlot(it)
        }

        val objective = teamObjective ?: scoreboard.getObjectiveForSlot(ScoreboardDisplaySlot.SIDEBAR)
            ?: return null

        val formatting = when (objective.numberFormat) {
            StyledNumberFormat.YELLOW -> Formatting.YELLOW
            StyledNumberFormat.EMPTY -> Formatting.RESET
            else -> Formatting.RED
        }

        return ObjectiveInfo(
            id = objective.name,
            displayName = objective.displayName,
            renderType = when (objective.numberFormat) {
                is BlankNumberFormat -> ScoreRenderType.Blank
                is FixedNumberFormat -> ScoreRenderType.Fixed(
                    (objective.numberFormat as NumberFormat).format(0)
                )
                else -> when (objective.renderType) {
                    ScoreboardCriterion.RenderType.HEARTS -> ScoreRenderType.Hearts(formatting)
                    else -> ScoreRenderType.Number(formatting)
                }
            },
        )
    }

    private val entryComparator = compareByDescending<ScoreboardEntry> { it.value }
        .thenComparing { o1, o2 ->
            String.CASE_INSENSITIVE_ORDER.compare(o1.owner, o2.owner)
        }

    override fun getScoreList(objectiveInfo: ObjectiveInfo): List<ScoreInfo> {
        val client = MinecraftClient.getInstance()
        val scoreboard = client.world?.scoreboard ?: return emptyList()
        val objective = scoreboard.getNullableObjective(objectiveInfo.id) ?: return emptyList()
        
        val scores = scoreboard.getScoreboardEntries(objective)
            .filter { !it.owner.startsWith("#") }
            .sortedWith(entryComparator)
            .take(15)

        return scores.map { score ->
            val team = scoreboard.getScoreHolderTeam(score.owner)
            val displayName = Team.decorateName(team, score.name())

            ScoreInfo(
                score.owner,
                displayName,
                score.value,
            )
        }
    }

    override fun getTeamColor(): Formatting? {
        val client = MinecraftClient.getInstance()
        return client.player?.scoreboardTeam?.color
    }
}