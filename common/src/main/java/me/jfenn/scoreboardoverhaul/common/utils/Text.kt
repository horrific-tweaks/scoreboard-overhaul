package me.jfenn.scoreboardoverhaul.common.utils

import me.jfenn.scoreboardoverhaul.common.client
import net.minecraft.client.font.TextRenderer
import net.minecraft.text.MutableText
import net.minecraft.text.Style
import net.minecraft.text.Text
import net.minecraft.util.Unit
import java.util.*

fun Text.shortenToWidth(
    textRenderer: TextRenderer,
    maxWidth: Int,
) : MutableText {
    val parts = mutableListOf<MutableText>()
    visit<Unit>({ style, asString ->
        if (asString.isNotEmpty()) {
            parts.add(Text.literal(asString).setStyle(style))
        }
        Optional.empty()
    }, Style.EMPTY)

    // Shorten the text until it is below the width
    while (
        parts.isNotEmpty() &&
        parts.sumOf { textRenderer.getWidth(it) } > maxWidth
    ) {
        // Remove a char from the last part of text
        val textPart = parts.removeLast()
        val str = textPart.string.dropLast(1)

        if (str.isNotEmpty()) {
            parts.add(Text.literal(str).setStyle(textPart.style))
        }
    }

    var text = Text.empty()
    for (part in parts)
        text = text.append(part)

    return text
}

fun Text.ellipsizeToWidth(
    textRenderer: TextRenderer,
    maxWidth: Int,
) : MutableText {
    val ellipsis = Text.literal("...")
    val ellipsisWidth = client.textRenderer.getWidth(ellipsis)

    return shortenToWidth(textRenderer, maxWidth - ellipsisWidth)
        .append(ellipsis)
}

fun Text.stringWithoutFormatting(): String {
    val builder = StringBuilder()
    visit<Unit>({ _, asString ->
        builder.append(asString)
        Optional.empty()
    }, Style.EMPTY)

    return builder.toString()
        .replace(Regex("§[0-9a-z]"), "")
}

fun Text.isBlank(): Boolean {
    return stringWithoutFormatting().isBlank()
}

fun Text.equalsIgnoringFormatting(other: Text): Boolean {
    val text1 = this.stringWithoutFormatting().trim()
    val text2 = other.stringWithoutFormatting().trim()
    return text1 == text2
}
