package me.jfenn.scoreboardoverhaul.common.data

import net.minecraft.text.Text

data class ObjectiveInfo(
    val id: String,
    val displayName: Text,
    val renderType: ScoreRenderType,
)
