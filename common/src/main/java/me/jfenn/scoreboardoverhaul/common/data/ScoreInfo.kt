package me.jfenn.scoreboardoverhaul.common.data

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import net.minecraft.text.Text

data class ScoreInfo(
    val id: String,
    val displayName: Text,
    val value: Int,
    var updatedAt: Instant = Clock.System.now(),
)
