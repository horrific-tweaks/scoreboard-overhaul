package me.jfenn.scoreboardoverhaul.common.hud

import kotlinx.datetime.Clock
import me.jfenn.scoreboardoverhaul.api.IConfigFactory
import me.jfenn.scoreboardoverhaul.api.IDrawService
import me.jfenn.scoreboardoverhaul.api.IHudCallback
import me.jfenn.scoreboardoverhaul.api.IOptionsAccessor
import me.jfenn.scoreboardoverhaul.common.TEXTURE_CONTAINER_PREFIX
import me.jfenn.scoreboardoverhaul.common.TEXTURE_TITLE
import me.jfenn.scoreboardoverhaul.common.client
import me.jfenn.scoreboardoverhaul.common.data.ObjectiveInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreInfo
import me.jfenn.scoreboardoverhaul.common.utils.ellipsizeToWidth
import me.jfenn.scoreboardoverhaul.common.utils.lerpNumber
import me.jfenn.scoreboardoverhaul.common.utils.lerpTo
import net.minecraft.client.gui.DrawContext
import net.minecraft.util.Identifier
import java.awt.Color
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.seconds

class HudRenderer(
    configFactory: IConfigFactory,
    private val state: HudState,
    private val options: IOptionsAccessor,
    private val hudCallback: IHudCallback,
) {

    private val config by configFactory

    companion object {
        const val SIDEBAR_PADDING = 5
        const val SIDEBAR_ENTRY_HEIGHT = 11
        const val SIDEBAR_ENTRY_MARGIN = 4
        const val SIDEBAR_TITLE_PADDING = 8

        val UPDATE_HIGHLIGHT_TIME = 3.seconds
    }

    private var hudVisibility: Float = 1f

    private val colorBackground get() = Color(config.backgroundColor, true)
    private val colorUpdated get() = Color(config.updateHighlightColor, true)
    private val colorTransparent get() = Color(colorUpdated.red, colorUpdated.green, colorUpdated.blue, 0)

    private fun updateVisibility() {
        this.hudVisibility = lerpNumber(hudVisibility, if (state.isVisible) 1f else 0f, 0.25f)

        // This should never happen... but just in case :)
        if (!hudVisibility.isFinite())
            hudVisibility = 0f
    }

    private fun shouldRenderFlicker(): Boolean {
        return (Clock.System.now().toEpochMilliseconds() % 500L).toInt() < 100
    }

    private fun measureScoreWidth(
        score: ScoreInfo,
    ): Int {
        val scoreValue = state.getRenderType().format(score.value, state.isExpanded || !config.truncateScoreValues)
        val scoreValueWidth = client.textRenderer.getWidth(scoreValue)
        val textWidth = client.textRenderer.getWidth(score.displayName)
        return scoreValueWidth + textWidth + SIDEBAR_ENTRY_MARGIN*3
    }

    private fun drawScoreEntry(
        context: DrawContext,
        score: ScoreInfo,
        x: Int,
        y: Int,
        hudWidth: Int,
    ) {
        val timeSinceUpdated = Clock.System.now() - score.updatedAt
        val ratio = timeSinceUpdated.inWholeMilliseconds.toFloat() / UPDATE_HIGHLIGHT_TIME.inWholeMilliseconds
        if (ratio < 1f) {
            if (config.updateHighlight) {
                val fillColor = colorUpdated.lerpTo(colorTransparent, ratio).rgb
                context.fill(x, y, x + hudWidth, y + SIDEBAR_ENTRY_HEIGHT, fillColor)
            }

            if (config.updateFlicker && shouldRenderFlicker()) return
        }

        val scoreValue = state.getRenderType().format(score.value, state.isExpanded || !config.truncateScoreValues)
        val scoreValueWidth = client.textRenderer.getWidth(scoreValue)
        context.drawText(
            client.textRenderer,
            scoreValue,
            x + hudWidth - SIDEBAR_ENTRY_MARGIN - scoreValueWidth, y + 1,
            -1,
            false
        )

        val textWidth = client.textRenderer.getWidth(score.displayName)
        val textWidthMax = hudWidth - (SIDEBAR_ENTRY_MARGIN*3) - scoreValueWidth
        val text = if (textWidth > textWidthMax) {
            score.displayName.ellipsizeToWidth(client.textRenderer, textWidthMax)
        } else score.displayName

        context.drawText(client.textRenderer, text, x + SIDEBAR_ENTRY_MARGIN, y + 1, -1, false)
    }

    /**
     * Draws the score title
     * NOTE: this is drawn horizontally-centered on the [x] axis
     */
    private fun drawScoreTitle(
        context: DrawContext,
        drawService: IDrawService,
        objectiveInfo: ObjectiveInfo,
        x: Int,
        y: Int,
    ) {
        val textWidth = client.textRenderer.getWidth(objectiveInfo.displayName)
        val titleX = x - textWidth/2
        val titleY = y - 4

        drawService.drawNinePatch(
            TEXTURE_TITLE,
            titleX - SIDEBAR_TITLE_PADDING,
            titleY - SIDEBAR_TITLE_PADDING,
            textWidth + SIDEBAR_TITLE_PADDING*2,
            9 + SIDEBAR_TITLE_PADDING*2,
            4,
            24, 24,
        )

        context.drawText(client.textRenderer, objectiveInfo.displayName, titleX, titleY, -1, false)
    }

    private fun drawHud(
        drawService: IDrawService,
    ) {
        if (hudVisibility < 0.01f) return
        val objectiveInfo = state.objectiveInfo ?: return
        if (options.isDebugEnabled()) return

        val context = drawService.context

        context.matrices.push()
        context.matrices.scale(config.scaleFloat, config.scaleFloat, config.scaleFloat)

        val windowWidth = context.scaledWindowWidth / config.scaleFloat
        val windowHeight = context.scaledWindowHeight / config.scaleFloat

        val titleWidth = client.textRenderer.getWidth(objectiveInfo.displayName) + SIDEBAR_TITLE_PADDING*2
        val scoreWidthMax = state.scores.maxOfOrNull { measureScoreWidth(it) } ?: titleWidth

        val hudWidth = SIDEBAR_PADDING*2 + max(
            titleWidth,
            when {
                state.isExpanded || !config.truncateScoreNames -> max(config.minWidth, scoreWidthMax)
                else -> min(config.maxWidth, scoreWidthMax)
            },
        )
        val hudHeight = SIDEBAR_ENTRY_HEIGHT + (state.scores.size * SIDEBAR_ENTRY_HEIGHT) + SIDEBAR_PADDING*2 + SIDEBAR_ENTRY_MARGIN*2

        val positionX = ((config.positionX / 100f) * windowWidth).minus(hudWidth/2).coerceIn(SIDEBAR_PADDING.toFloat(), windowWidth - hudWidth - SIDEBAR_PADDING)
        val positionY = ((config.positionY / 100f) * windowHeight).minus(hudHeight/2).coerceIn(SIDEBAR_PADDING.toFloat() + 12, windowHeight - hudHeight - SIDEBAR_PADDING)

        val offScreenX = if (config.positionX > 50) windowWidth + hudWidth else -hudWidth
        val hudX = lerpNumber(offScreenX, positionX, hudVisibility).roundToInt()
        val hudY = positionY.toInt()

        // Draw score background
        val hudBgInset = 4
        context.fill(
            hudX + hudBgInset, hudY + hudBgInset,
            hudX + hudWidth - hudBgInset, hudY + hudHeight - hudBgInset,
            colorBackground.rgb,
        )
        /*
        // This causes a weird bug with the background alpha that I can't be bothered to fix
        drawService.drawNinePatch(
            TEXTURE_BACKGROUND,
            hudX + hudBgInset, hudY + hudBgInset,
            hudWidth - hudBgInset*2, hudHeight - hudBgInset*2,
            1,
            24, 24,
        )*/

        // Draw score entries
        state.scores.forEachIndexed { i, score ->
            drawScoreEntry(
                context = context,
                score = score,
                x = hudX + SIDEBAR_PADDING,
                y = hudY + SIDEBAR_PADDING + SIDEBAR_ENTRY_MARGIN + (i + 1) * SIDEBAR_ENTRY_HEIGHT,
                hudWidth = hudWidth - SIDEBAR_PADDING*2,
            )
        }

        // Draw the sidebar border
        drawService.drawNinePatch(
            TEXTURE_CONTAINER_PREFIX.let { Identifier.of(it.namespace, it.path + state.teamColor?.name?.lowercase())!! },
            hudX, hudY,
            hudWidth, hudHeight,
            6,
            24, 24,
        )

        drawScoreTitle(context, drawService, objectiveInfo, hudX + hudWidth/2, hudY)

        context.matrices.pop()
    }

    fun init() {
        hudCallback.onRender {
            if (!config.isEnabled) return@onRender
            if (options.isHudHidden()) return@onRender
            updateVisibility()
            drawHud(it.drawService)
        }
    }
}