package me.jfenn.scoreboardoverhaul.common.data

import net.minecraft.text.Style
import net.minecraft.text.Text
import net.minecraft.util.Formatting

sealed class ScoreRenderType {
    data class Number(
        val formatting: Formatting = Formatting.RED,
        val style: Style = Style.EMPTY.withFormatting(formatting),
    ) : ScoreRenderType() {
        override fun format(value: Int, isExpanded: Boolean): Text {
            val number = when {
                isExpanded -> "%,d".format(value)
                value >= 1000000 -> "${value / 1000000}M"
                value >= 1000 -> "${value / 1000}K"
                else -> value.toString()
            }

            return Text.literal(number).fillStyle(style)
        }
    }

    data class Hearts(
        val formatting: Formatting = Formatting.RED,
        val style: Style = Style.EMPTY.withFormatting(formatting),
    ) : ScoreRenderType() {
        override fun format(value: Int, isExpanded: Boolean): Text {
            val str = "♥".repeat(value.coerceAtLeast(0))
            return Text.literal(str).fillStyle(style)
        }
    }

    data class Fixed(val text: Text) : ScoreRenderType() {
        override fun format(value: Int, isExpanded: Boolean): Text {
            return text
        }
    }

    data object Blank : ScoreRenderType() {
        override fun format(value: Int, isExpanded: Boolean) : Text {
            return Text.empty()
        }
    }

    abstract fun format(value: Int, isExpanded: Boolean) : Text

}
