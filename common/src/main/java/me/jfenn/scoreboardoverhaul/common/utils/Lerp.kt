package me.jfenn.scoreboardoverhaul.common.utils

fun lerpNumber(from: Number, to: Number, ratio: Float): Float {
    val r = ratio.coerceIn(0f, 1f)
    return from.toFloat() + r * (to.toFloat() - from.toFloat())
}
