package me.jfenn.scoreboardoverhaul.common

import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper
import net.minecraft.client.MinecraftClient
import net.minecraft.client.option.KeyBinding
import net.minecraft.client.util.InputUtil
import org.lwjgl.glfw.GLFW
import org.slf4j.LoggerFactory

const val MOD_ID = "scoreboard-overhaul"

val client get() = MinecraftClient.getInstance()

object Main : ClientModInitializer {

    val log = LoggerFactory.getLogger(this::class.java)

    val scoreboardExpandKey = KeyBindingHelper.registerKeyBinding(
        KeyBinding(
            "key.$MOD_ID.expand",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_UNKNOWN,
            "category.$MOD_ID"
        )
    )

    override fun onInitializeClient() {

    }
}