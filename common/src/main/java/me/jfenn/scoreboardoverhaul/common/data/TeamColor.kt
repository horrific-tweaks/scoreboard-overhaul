package me.jfenn.scoreboardoverhaul.common.data

enum class TeamColor {
    AQUA,
    BLACK,
    BLUE,
    DARK_AQUA,
    DARK_BLUE,
    DARK_GRAY,
    DARK_GREEN,
    DARK_PURPLE,
    DARK_READ,
    GOLD,
    GRAY,
    GREEN,
    LIGHT_PURPLE,
    RED,
    WHITE,
    YELLOW,
}
