package me.jfenn.scoreboardoverhaul.common.utils

import java.awt.Color
import kotlin.math.roundToInt

/**
 * Lerps from [this] to [other] as the [ratio] argument progresses from 0 to 1
 */
fun Color.lerpTo(other: Color, ratio: Float): Color {
    return Color(
        lerpNumber(red, other.red, ratio).roundToInt(),
        lerpNumber(green, other.green, ratio).roundToInt(),
        lerpNumber(blue, other.blue, ratio).roundToInt(),
        lerpNumber(alpha, other.alpha, ratio).roundToInt(),
    )
}

fun Color.luminescence(): Float {
    return 0.2126f * red + 0.7152f * green + 0.0722f * blue
}
