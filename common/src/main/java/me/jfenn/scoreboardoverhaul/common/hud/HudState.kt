package me.jfenn.scoreboardoverhaul.common.hud

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import me.jfenn.scoreboardoverhaul.api.IConfig
import me.jfenn.scoreboardoverhaul.api.IConfigFactory
import me.jfenn.scoreboardoverhaul.api.IOptionsAccessor
import me.jfenn.scoreboardoverhaul.api.IScoreboardAccessor
import me.jfenn.scoreboardoverhaul.common.Main
import me.jfenn.scoreboardoverhaul.common.client
import me.jfenn.scoreboardoverhaul.common.data.ObjectiveInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreRenderType
import me.jfenn.scoreboardoverhaul.common.data.TeamColor
import me.jfenn.scoreboardoverhaul.common.utils.FrequencySampler
import me.jfenn.scoreboardoverhaul.common.utils.equalsIgnoringFormatting
import me.jfenn.scoreboardoverhaul.common.utils.isBlank
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents
import net.minecraft.client.sound.PositionedSoundInstance
import net.minecraft.sound.SoundEvents
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class HudState(
    configFactory: IConfigFactory,
    private val scoreboardAccessor: IScoreboardAccessor,
    private val optionsAccessor: IOptionsAccessor,
) {
    private val config by configFactory

    var isVisible: Boolean = true
    var isExpanded: Boolean = false
    var objectiveInfo: ObjectiveInfo? = null
    var entries: Map<String, ScoreInfo> = emptyMap()
    var teamColor: TeamColor? = null
    var updatedAt: Instant = Clock.System.now()
    var changedAt: Instant = Clock.System.now()

    // Monitor entry update frequency over the past 10 seconds (* 20 TPS)
    // - if a scoreboard entry gets at least one update per second for 10 seconds,
    //  it should update silently (without activating the scoreboard)
    private val frequencyThreshold = 0.8f / 20f // this is slightly less than 1/20 (once per second) to account for lag
    private val frequencyById = FrequencySampler(10 * 20)
    private val frequencyByScore = FrequencySampler(10 * 20)

    private var debounceEntries: Map<String, ScoreInfo> = emptyMap()
    private var debounceTime: Instant? = null
    private var debounceDuration = 0.5.seconds

    private val updateVisibleTime get() = config.showDuration.seconds

    val scores get() = entries.values.sortedByDescending { it.value }

    fun getRenderType(): ScoreRenderType {
        if (config.showScoreValues == IConfig.ShowScores.Never) {
            return ScoreRenderType.Blank
        }

        val scoreNumbers = entries.values.map { it.value }.sorted()
        val scoreMin = scoreNumbers.firstOrNull()

        // Detect if the scoreboard is only using numbers for display purposes
        if (
            config.showScoreValues == IConfig.ShowScores.Auto &&
            objectiveInfo?.renderType is ScoreRenderType.Number &&
            // If there is a blank score entry
            scores.any { it.displayName.isBlank() } &&
            // And there is more than one score
            scores.size > 1 &&
            // And the score starts at 0 or 1
            scoreMin in 0..1 &&
            // And *most* scores are contiguous
            // (allowing up to two non-contiguous scores, since client updates can be split between ticks)
            scoreNumbers.withIndex()
                .drop(1)
                .filterNot { (i, value) -> scoreNumbers[i-1] + 1 == value }
                .size < 2
        ) {
            return ScoreRenderType.Blank
        }

        return objectiveInfo?.renderType ?: ScoreRenderType.Number()
    }

    private fun updateVisibility() {
        // If there is no objective being displayed or we aren't in a world, don't do anything!
        if (this.objectiveInfo == null || client.world == null) {
            this.isVisible = false
            return
        }

        val isManuallyOpened = Main.scoreboardExpandKey.isPressed || optionsAccessor.isPlayerListPressed()
        this.isExpanded = isManuallyOpened
        if (isManuallyOpened || config.isAlwaysVisible) {
            this.isVisible = true
            return
        }

        val timeSinceUpdated = Clock.System.now() - this.changedAt
        val isVisible = timeSinceUpdated < updateVisibleTime
        when {
            isVisible && !this.isVisible -> {
                client.soundManager.play(PositionedSoundInstance.master(SoundEvents.UI_TOAST_IN, 1.0f, 1.0f))
            }
            !isVisible && this.isVisible -> {
                client.soundManager.play(PositionedSoundInstance.master(SoundEvents.UI_TOAST_OUT, 1.0f, 1.0f))
            }
        }

        this.isVisible = isVisible
    }

    private fun dedupeScores(
        previousEntries: Map<String, ScoreInfo>,
        newEntries: Map<String, ScoreInfo>,
    ): Map<String, ScoreInfo> {
        val ret = newEntries.mapValues { (_, score) -> score.copy() }

        for (score in ret.values) {
            val prevScore = previousEntries[score.id]
            if (
                prevScore != null &&
                prevScore.displayName.equalsIgnoringFormatting(score.displayName) &&
                (getRenderType() == ScoreRenderType.Blank || prevScore.value == score.value)
            ) {
                // If the score is equal to an existing score, it retains the last updatedAt value
                score.updatedAt = prevScore.updatedAt
            }
        }

        return ret
    }

    /**
     * Compares [previousEntries] to [newEntries] and returns "true" if they
     * should be considered as changed.
     */
    private fun compareEntries(
        previousEntries: Map<String, ScoreInfo>,
        newEntries: Map<String, ScoreInfo>,
    ): Boolean {
        var isDirty = false

        for (score in newEntries.values) {
            val prevScore = previousEntries[score.id]
            if (
                prevScore == null ||
                !prevScore.displayName.equalsIgnoringFormatting(score.displayName) ||
                (getRenderType() != ScoreRenderType.Blank && prevScore.value != score.value)
            ) {
                if (
                    !frequencyById.isAboveThreshold(score.id, frequencyThreshold) &&
                    !frequencyByScore.isAboveThreshold(score.value.toString(), frequencyThreshold)
                ) {
                    isDirty = true
                }
            }
        }

        // If any scores were removed, mark the scoreboard as dirty
        val isScoreRemoved = previousEntries
            .filter { !newEntries.containsKey(it.key) }
            .filter { (_, entry) ->
                !frequencyById.isAboveThreshold(entry.id, frequencyThreshold)
            }
            .filter { (_, entry) ->
                !frequencyByScore.isAboveThreshold(entry.value.toString(), frequencyThreshold)
            }
            .isNotEmpty()

        if (isScoreRemoved)
            isDirty = true

        return isDirty
    }

    private fun updateState() {
        // Tick the entry update frequency sampler
        frequencyById.tick()
        frequencyByScore.tick()

        val objectiveInfo = scoreboardAccessor.getSidebarObjective()
        var isDirty = false

        // If the sidebar objective has changed
        if (objectiveInfo != this.objectiveInfo) {
            frequencyById.clear()
            frequencyByScore.clear()
            this.objectiveInfo = objectiveInfo
            isDirty = true
        }

        val teamColor = scoreboardAccessor.getTeamColor()
            ?.name
            ?.let { colorName -> TeamColor.entries.find { it.name == colorName } }

        if (teamColor != this.teamColor) {
            this.teamColor = teamColor
            isDirty = true
        }

        if (objectiveInfo != null) {
            val newEntries = scoreboardAccessor.getScoreList(objectiveInfo)
                .associateBy { it.id }
                .let { dedupeScores(this.entries, it) }

            // Get all newly updated scores...
            newEntries.values
                .filter { it.updatedAt > this.updatedAt }
                .onEach { score ->
                    // If the score entry is being changed frequently, update it silently...
                    if (
                        frequencyById.isAboveThreshold(score.id, frequencyThreshold) ||
                        frequencyByScore.isAboveThreshold(score.value.toString(), frequencyThreshold)
                    ) {
                        val updatedAt = this.entries[score.id]?.updatedAt
                            ?: this.scores.find { it.value == score.value }?.updatedAt
                            ?: (score.updatedAt - 1.minutes)

                        score.updatedAt = updatedAt
                    }
                }
                .onEach { score ->
                    // If the score is changed, add it to the frequency trackers
                    frequencyById.addHit(score.id)
                    frequencyByScore.addHit(score.value.toString())
                }


            // We want to ensure that any new entries are actually changed before setting isDirty.
            //
            // In order to ensure this, we put any changes through a "debouncing" period before updating
            // the scoreboard.
            //
            // If the new scores are still different from the previous scores at the end of debouncing,
            // then we should activate the scoreboard.
            //
            // States: [Unchanged] -> [Debouncing] -> [Unchanged]
            //                                     -> [Updated] -> [Unchanged]

            // Detect if the scoreboard has changed...
            val isChanged = compareEntries(this.debounceEntries, newEntries)

            if (isChanged) {
                // If there is a change, we aren't debouncing yet...
                if (this.debounceTime == null) {
                    // Set a new debounceTime to keep checking new updates against the list
                    this.debounceTime = Clock.System.now()
                }

                // If there is still a change, and the debouncing duration has passed...
                if (
                    this.debounceTime?.let { Clock.System.now() - it > this.debounceDuration } == true
                ) {
                    // Set isDirty, and reset the debouncing state
                    if (config.showOnUpdate)
                        isDirty = true

                    this.debounceEntries = newEntries
                    this.debounceTime = null
                }
            } else {
                // Otherwise, regardless of whether we are debouncing
                // (i.e. if newEntries at any point is equal to debounceEntries while debouncing)
                // then reset the debouncing state
                this.debounceEntries = newEntries
                this.debounceTime = null
            }

            this.entries = newEntries
        }

        this.updatedAt = Clock.System.now()
        if (isDirty) {
            this.changedAt = Clock.System.now()
        }
    }

    fun init() {
        ClientTickEvents.END_CLIENT_TICK.register {
            if (!config.isEnabled || it.isPaused) return@register

            updateState()
            updateVisibility()
        }
    }

}