package me.jfenn.scoreboardoverhaul.common

import net.minecraft.util.Identifier

val TEXTURE_TITLE = Identifier.of("minecraft", "$MOD_ID/title")!!
val TEXTURE_BACKGROUND = Identifier.of("minecraft", "$MOD_ID/background")!!
val TEXTURE_CONTAINER_PREFIX = Identifier.of("minecraft", "$MOD_ID/container_")!!
