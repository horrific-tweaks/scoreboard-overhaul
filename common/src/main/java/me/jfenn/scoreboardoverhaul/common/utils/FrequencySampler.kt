package me.jfenn.scoreboardoverhaul.common.utils

import java.util.*

class FrequencySampler(
    private val bufferSize: Int,
) {

    // Map of ids to their frequency list
    private val frequencyMap = mutableMapOf<String, LinkedList<Boolean>>()

    // Set of all ids that have received a hit in the current tick
    private val hitSet = mutableSetOf<String>()

    fun addHit(id: String) {
        hitSet.add(id)
    }

    fun tick() {
        for (id in (frequencyMap.keys + hitSet).toSet()) {
            // Get (or insert) the frequency list for each id
            val frequencyList = frequencyMap.getOrPut(id) { LinkedList() }
            while (frequencyList.size < bufferSize) {
                frequencyList.add(false)
            }

            // Remove the first (oldest) element from each list
            frequencyList.pop()

            // Push a new item into the list for the current tick
            val wasHit = hitSet.contains(id)
            frequencyList.add(wasHit)
        }

        // Clear the hit flags for the next tick
        hitSet.clear()
    }

    fun clearFrequency(id: String) {
        frequencyMap.remove(id)
        hitSet.remove(id)
    }

    fun getFrequency(id: String) : Int {
        val frequencyList = frequencyMap[id] ?: return 0
        val frequency = frequencyList.count { it }

        // If there are no more hits, remove the list
        if (frequency == 0)
            frequencyMap.remove(id)

        return frequency
    }

    fun clear() {
        frequencyMap.clear()
        hitSet.clear()
    }

    /**
     * Returns whether the id has hits above the provided
     * [threshold] (range from 0..1) over the specified
     * [bufferSize].
     */
    fun isAboveThreshold(id: String, threshold: Float) : Boolean {
        val frequency = getFrequency(id)
        return (frequency.toFloat() / bufferSize) > threshold
    }

}