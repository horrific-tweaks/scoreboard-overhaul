package me.jfenn.scoreboardoverhaul.impl

import me.jfenn.scoreboardoverhaul.api.IConfig
import me.jfenn.scoreboardoverhaul.api.IConfigFactory

class ScoreboardConfigDummy : IConfig {
    override var isEnabled: Boolean = false
    override var isAlwaysVisible: Boolean = false
    override var showOnUpdate: Boolean = false
    override var showDuration: Int = 0
    override var backgroundColor: Int = 0
    override var scale: Int = 0
    override var positionX: Int = 0
    override var positionY: Int = 0
    override var updateFlicker: Boolean = false
    override var updateHighlight: Boolean = false
    override var updateHighlightColor: Int = 0
    override var minWidth: Int = 0
    override var maxWidth: Int = 0
    override var truncateScoreNames: Boolean = false
    override var truncateScoreValues: Boolean = false
    override var showScoreValues: IConfig.ShowScores = IConfig.ShowScores.Auto

    init {
        resetDefaults()
    }
}

object ScoreboardConfigDummyFactory : IConfigFactory {
    val config = ScoreboardConfigDummy()

    override fun read(): IConfig {
        return config
    }
}
