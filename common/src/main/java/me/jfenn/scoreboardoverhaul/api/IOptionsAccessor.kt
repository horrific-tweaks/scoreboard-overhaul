package me.jfenn.scoreboardoverhaul.api

interface IOptionsAccessor {

    fun isDebugEnabled(): Boolean

    fun isPlayerListPressed(): Boolean

    fun isHudHidden(): Boolean

}