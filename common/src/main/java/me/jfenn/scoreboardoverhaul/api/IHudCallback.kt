package me.jfenn.scoreboardoverhaul.api

import me.jfenn.scoreboardoverhaul.common.utils.EventListener

interface IHudCallback {
    val onRender: EventListener<RenderEvent>

    class RenderEvent(
        val drawService: IDrawService,
        val delta: Float,
    )
}