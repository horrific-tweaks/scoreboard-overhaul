package me.jfenn.scoreboardoverhaul.api

import java.awt.Color
import kotlin.reflect.KProperty

interface IConfig {
    var isEnabled: Boolean

    var isAlwaysVisible: Boolean
    var showOnUpdate: Boolean
    var showDuration: Int
    var backgroundColor: Int

    var scale: Int
    var positionX: Int
    var positionY: Int

    var updateFlicker: Boolean
    var updateHighlight: Boolean
    var updateHighlightColor: Int

    var truncateScoreNames: Boolean
    var truncateScoreValues: Boolean
    var minWidth: Int
    var maxWidth: Int

    enum class ShowScores { Always, Auto, Never }
    var showScoreValues: ShowScores

    fun resetDefaults() {
        isEnabled = true

        isAlwaysVisible = false
        showOnUpdate = true
        showDuration = 8
        backgroundColor = Color(0f, 0f, 0f, 0.5f).rgb

        scale = 100
        positionX = 100
        positionY = 50

        updateFlicker = false
        updateHighlight = true
        updateHighlightColor = Color(1f, 1f, 1f, 0.25f).rgb

        truncateScoreNames = true
        truncateScoreValues = true
        minWidth = 80
        maxWidth = 180

        showScoreValues = ShowScores.Auto
    }

    val scaleFloat get() = scale / 100f
}

interface IConfigFactory {
    fun read() : IConfig

    operator fun getValue(thisRef: Any?, property: KProperty<*>): IConfig = read()
}
