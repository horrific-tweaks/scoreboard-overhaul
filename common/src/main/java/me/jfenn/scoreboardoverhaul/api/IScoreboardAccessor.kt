package me.jfenn.scoreboardoverhaul.api

import me.jfenn.scoreboardoverhaul.common.data.ObjectiveInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreInfo
import net.minecraft.util.Formatting
import java.util.*

interface IScoreboardAccessor {
    fun getSidebarObjective(): ObjectiveInfo?
    fun getScoreList(objectiveInfo: ObjectiveInfo): List<ScoreInfo>
    fun getTeamColor(): Formatting?
}
