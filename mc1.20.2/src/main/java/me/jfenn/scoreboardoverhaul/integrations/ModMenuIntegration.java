package me.jfenn.scoreboardoverhaul.integrations;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import me.jfenn.scoreboardoverhaul.CommonKt;
import me.jfenn.scoreboardoverhaul.impl.ScoreboardConfigFactory;

public class ModMenuIntegration implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        if (CommonKt.hasClothConfig()) {
            return ScoreboardConfigFactory.INSTANCE::getScreen;
        } else {
            return screen -> null;
        }
    }
}
