package me.jfenn.scoreboardoverhaul.impl

import me.jfenn.scoreboardoverhaul.api.IScoreboardAccessor
import me.jfenn.scoreboardoverhaul.common.data.ObjectiveInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreInfo
import me.jfenn.scoreboardoverhaul.common.data.ScoreRenderType
import net.minecraft.client.MinecraftClient
import net.minecraft.scoreboard.ScoreboardCriterion
import net.minecraft.scoreboard.ScoreboardDisplaySlot
import net.minecraft.scoreboard.Team
import net.minecraft.text.Text
import net.minecraft.util.Formatting

class ScoreboardAccessor : IScoreboardAccessor {
    override fun getSidebarObjective(): ObjectiveInfo? {
        val client = MinecraftClient.getInstance()
        val scoreboard = client.world?.scoreboard ?: return null
        val player = client.player ?: return null

        val teamSlot = player.scoreboardTeam?.color?.let { ScoreboardDisplaySlot.fromFormatting(it) }
        val teamObjective = teamSlot?.let {
            scoreboard.getObjectiveForSlot(it)
        }

        val objective = teamObjective ?: scoreboard.getObjectiveForSlot(ScoreboardDisplaySlot.SIDEBAR)
            ?: return null

        return ObjectiveInfo(
            id = objective.name,
            displayName = objective.displayName,
            renderType = when (objective.renderType) {
                ScoreboardCriterion.RenderType.HEARTS -> ScoreRenderType.Hearts()
                else -> ScoreRenderType.Number()
            },
        )
    }

    override fun getScoreList(objectiveInfo: ObjectiveInfo): List<ScoreInfo> {
        val client = MinecraftClient.getInstance()
        val scoreboard = client.world?.scoreboard ?: return emptyList()
        val objective = scoreboard.getNullableObjective(objectiveInfo.id) ?: return emptyList()

        val scores = scoreboard.getAllPlayerScores(objective)
            .filter { it.playerName != null && !it.playerName.startsWith("#") }
            .reversed()
            .take(15)

        return scores.map { score ->
            val team = scoreboard.getPlayerTeam(score.playerName)
            ScoreInfo(
                score.playerName,
                Team.decorateName(team, Text.literal(score.playerName)),
                score.score,
            )
        }
    }

    override fun getTeamColor(): Formatting? {
        val client = MinecraftClient.getInstance()
        return client.player?.scoreboardTeam?.color
    }
}