# Scoreboard Overhaul

[![Requires Fabric API](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/requires/fabric-api_vector.svg)](https://modrinth.com/mod/fabric-api)
[![Available on GitLab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/horrific-tweaks/scoreboard-overhaul)
[![Available on Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/mod/scoreboard-overhaul)

This mod redesigns the Minecraft scoreboard/sidebar to make it easier to use and somewhat better looking!

Make sure to also download the [Fabric API](https://modrinth.com/mod/fabric-api)! You'll also want to install [Mod Menu](https://modrinth.com/mod/modmenu) and [Cloth Config](https://modrinth.com/mod/cloth-config) to change any of the settings (but these are not required for it to run).

![A screenshot of a scoreboard with a pink outline showing BINGO information](https://gitlab.com/horrific-tweaks/scoreboard-overhaul/-/raw/main/docs/screenshot.png)

### Features

- **Auto-hides the scoreboard when it isn't being used**

  By default, the scoreboard is shown for a brief period when either a score is changed or the Tab key is pressed.
  This can be configured in the settings!

- **Highlights score entries when updated**

  When a score is updated, it has a background "highlight" for a brief period.
  There is an additional option to make scores flash when updated to make them more noticeable (turned off by default).

- **Shows the player's current team color**

  If the player is on a team, the scoreboard border shows their team's color!

### Config Options

> **Note:** Options can only be configured if both [Mod Menu](https://modrinth.com/mod/modmenu) and [Cloth Config](https://modrinth.com/mod/cloth-config) are installed!

![A settings page with the default configured scoreboard options](https://gitlab.com/horrific-tweaks/scoreboard-overhaul/-/raw/main/docs/options.png?v=1)

- **Enable Scoreboard Overhaul:** Controls whether the mod is active (if turned off, uses the vanilla scoreboard).
- **Always show scoreboard:** Overrides the dynamic hiding behavior, and makes the scoreboard always visible (like the vanilla scoreboard).
- **Show scoreboard when changed:** Displays the scoreboard for a brief amount of time whenever a score is changed.
- **Delay until the scoreboard hides:** How long the scoreboard should remain visible for after being shown.
- **Background color:** Background color of the scoreboard content
- **Scoreboard size:** The size of the scoreboard, in percent (relative to the current GUI scale)
- **Horizontal position:** The horizontal alignment of the scoreboard, in percent (0 is left, 100 is right)
- **Vertical position:** The vertical alignment of the scoreboard, in percent (0 is top, 100 is bottom)
- **Flash scores when changed:** Whether scores should flicker to indicate which entries have changed.
- **Highlight scores when changed:** Whether scores should be highlighted to indicate which entries have changed.
- **Highlight color:** The color to highlight changed scoreboard items with.
