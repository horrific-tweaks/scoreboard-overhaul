package me.jfenn.scoreboardoverhaul.mixin;

import me.jfenn.scoreboardoverhaul.Main;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.scoreboard.ScoreboardObjective;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(InGameHud.class)
public abstract class InGameHudMixin {

    @Inject(
            method = "renderScoreboardSidebar",
            at = @At(value = "HEAD"),
            cancellable = true
    )
    private void renderScoreboardSidebar(DrawContext context, ScoreboardObjective objective, CallbackInfo ci) {
        if (Main.INSTANCE.getConfigFactory().read().isEnabled()) {
            // Don't render the client scoreboard at all
            ci.cancel();
        }
    }

}
