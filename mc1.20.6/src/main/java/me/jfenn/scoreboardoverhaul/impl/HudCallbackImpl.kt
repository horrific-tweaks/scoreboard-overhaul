package me.jfenn.scoreboardoverhaul.impl

import me.jfenn.scoreboardoverhaul.api.IDrawServiceFactory
import me.jfenn.scoreboardoverhaul.api.IHudCallback
import me.jfenn.scoreboardoverhaul.common.utils.EventListener
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback

class HudCallbackImpl(
    private val drawServiceFactory: IDrawServiceFactory,
) : IHudCallback {
    override val onRender = EventListener<IHudCallback.RenderEvent>()

    init {
        HudRenderCallback.EVENT.register { drawContext, delta ->
            onRender.invoke(
                IHudCallback.RenderEvent(
                    drawService = drawServiceFactory.create(drawContext),
                    delta = delta
                )
            )
        }
    }
}