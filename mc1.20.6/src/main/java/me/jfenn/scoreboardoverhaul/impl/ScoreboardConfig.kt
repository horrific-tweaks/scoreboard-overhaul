package me.jfenn.scoreboardoverhaul.impl

import me.jfenn.scoreboardoverhaul.api.IConfig
import me.jfenn.scoreboardoverhaul.api.IConfigFactory
import me.jfenn.scoreboardoverhaul.common.MOD_ID
import me.shedaniel.autoconfig.AutoConfig
import me.shedaniel.autoconfig.ConfigData
import me.shedaniel.autoconfig.annotation.Config
import me.shedaniel.autoconfig.annotation.ConfigEntry
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer
import net.minecraft.client.gui.screen.Screen


@Config(name = MOD_ID)
class ScoreboardConfig : IConfig, ConfigData {
    override var isEnabled: Boolean = false
    override var isAlwaysVisible: Boolean = false
    override var showOnUpdate: Boolean = false
    @ConfigEntry.BoundedDiscrete(min = 1, max = 60)
    override var showDuration: Int = 0
    @ConfigEntry.ColorPicker(allowAlpha = true)
    override var backgroundColor: Int = 0

    @ConfigEntry.BoundedDiscrete(min = 20, max = 300)
    override var scale: Int = 0
    @ConfigEntry.BoundedDiscrete(min = 0, max = 100)
    override var positionX: Int = 0
    @ConfigEntry.BoundedDiscrete(min = 0, max = 100)
    override var positionY: Int = 0

    override var updateFlicker: Boolean = false
    override var updateHighlight: Boolean = false
    @ConfigEntry.ColorPicker(allowAlpha = true)
    override var updateHighlightColor: Int = 0

    override var truncateScoreNames: Boolean = false
    override var truncateScoreValues: Boolean = true
    @ConfigEntry.BoundedDiscrete(min = 0, max = 500)
    override var minWidth: Int = 0
    @ConfigEntry.BoundedDiscrete(min = 80, max = 500)
    override var maxWidth: Int = 0

    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    override var showScoreValues: IConfig.ShowScores = IConfig.ShowScores.Auto

    init {
        resetDefaults()
    }
}

object ScoreboardConfigFactory : IConfigFactory {
    init {
        AutoConfig.register(ScoreboardConfig::class.java, ::GsonConfigSerializer)
    }

    override fun read(): IConfig {
        return AutoConfig.getConfigHolder(ScoreboardConfig::class.java).config
    }

    fun getScreen(parent: Screen): Screen {
        return AutoConfig.getConfigScreen(ScoreboardConfig::class.java, parent).get()
    }
}
